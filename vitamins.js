const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];


// 1. Get all items that are available 

function itemAvailable(arr) {
    let item = arr.filter(curr => curr.available == true);
    return item;
}
console.log(itemAvailable(items));


// 2. Get all items containing only Vitamin C.

function onlyVitaminC(arr) {
    let VitaminC = arr.filter(curr => curr.contains === "Vitamin C");
    return VitaminC;
}
console.log(onlyVitaminC(items));


// 3. Get all items containing Vitamin A.

function containVitaminA(arr) {
    let vitaminA = arr.filter(curr => curr["contains"].includes("Vitamin A"));
    return vitaminA;
}
console.log(containVitaminA(items));


//  4. Group items based on the Vitamins that they contain in the following format:
// {
//     "Vitamin C": ["Orange", "Mango"],
//     "Vitamin K": ["Mango"],
// }

// function vitamins(arr) {
//     let vitamin = arr.reduce((acc,curr) => {
//         if(acc[curr.contains]){
//             acc[curr.contains] += acc[curr.name];
//         }
//         else{
//             acc[curr.contains] = acc[curr.nmae];
//         }
//         return acc;
//     },{})
//     return vitamin;
// }
// console.log(vitamins(items));


// 5. Sort items based on number of Vitamins they contain

function vitaminsSortedBasedOnContains(arr) {
    let sortedVitamins = arr.sort((val1, val2) => {
        let num1 = val1.contains.split(',').length;
        let num2 = val2.contains.split(',').length;
        return num1 > num2 ? -1 : 1;
    });
    return sortedVitamins;
}
console.log(vitaminsSortedBasedOnContains(items));